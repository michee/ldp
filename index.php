<?php
// j'ouvre une session
 session_start();

// Fonction permettant le chargement automatique des classes
spl_autoload_register(function ($className) {
    require_once './config/classes/' . $className . '.php';
});
require_once './config/functions/autoLoad.php';
autoLoad("*.php");
require __DIR__ . '/vendor/autoload.php';

// Définir le fuseau horaire dans lequel le serveur se trouve
date_default_timezone_set('Europe/Paris');

/* Utiliser include ou require
* include renvoie un avertissement simple en cas d'erreur
* require renvoie une erreur fatale et arrête l'exécution du script
*/

if (verifierAdmin())
    require_once './includes/admin/headerAdmin.php';
else
    require_once './includes/header.php';

require_once './includes/main.php';
require_once './includes/footer.php';
$page = 1;
$itemPerpage = 5;
$offset = 0;
if (!empty($_GET['page']) && ctype_digit($_GET['page'])) {
    $page = $_GET['page'];
    $offset = ($page - 1) * $itemPerpage;
}

// $sql = "SELECT * FROM articles WHERE status = 'publish' ORDER BY created_at DESC LIMIT $itemPerpage OFFSET $offset";
// $query = $pdo->prepare($sql);
// $query->execute();
// $articles = $query->fetchAll();

// $count = countArticle();

// include('includes/header.php'); ?>
<!-- // <div class="wrap">
//     <h1>Blog</h1>
//     <?= pagination($page, $itemPerpage, $count); ?>
//     <section id="articles">
//         <?php foreach ($articles as $article) { ?>
//             <div class="article">
//                 <h2><?= $article['title']; ?></h2>
//                 <a href="../ldp/includes/post/singlePost.php ?id=<?= $article['id']; ?>">
//                     <img src="https://picsum.photos/id/<?= $article['id'] + 45; ?>/300/200" alt="">
//                 </a>
//             </div>
//         <?php } ?>
//     </section>
//    <?= pagination($page, $itemPerpage, $count); ?>
// </div> --> 
