-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3006
-- Généré le : jeu. 11 août 2022 à 14:00
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `idarticles` int(11) NOT NULL,
  `title` varchar(105) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `status` varchar(45) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`idarticles`, `title`, `content`, `created_at`, `modified_at`, `status`, `category_id`) VALUES
(1, 'La rumeur', 'Comment faire courir une rumeur et pousser la victime au suicide', '0000-00-00 00:00:00', '2022-08-08 23:32:46', 'public', 4),
(2, 'dictionnaire des vannes', 'rien de telle qu\'une bonne vanne qui fait mal', '2022-08-04 01:17:35', '2022-08-10 16:02:29', 'public', 0),
(10, 'vivre heureux', 'pour vivre heureux vivons comme ces connards de cassos', '2022-08-08 17:14:36', '2022-08-09 11:12:30', 'public', 0),
(11, 'parents responsables', 'Corriger son erreur en pratiquant l\'infanticide.\r\nConseil pour crever son putain de gosse pourri gâté sans se faire prendre.', '2022-08-08 23:53:00', '2022-08-09 10:17:36', 'prive', 0),
(14, 'jalousie', 'Elle avait tout moi rien.\r\nTant pis pour elle je l\'ai fait crever grâce à l\'antigel de voiture et du sirop de menthe. Cocktail maison.', '2022-08-09 10:59:37', '2022-08-09 11:00:46', 'public', 1),
(15, 'Cocu Zen', 'Méthode pour vivre sa vengeance sereinement.', '2022-08-09 11:04:52', '2022-08-09 12:11:13', 'prive', 2);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`category_id`, `name`) VALUES
(1, 'Meurtre'),
(2, 'Témoignage'),
(3, 'parent/enfant'),
(4, 'bien-être'),
(5, 'infidélité'),
(6, 'Travail');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `idcomments` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `articles_idarticles` int(11) NOT NULL,
  `comments_idcomments` int(11) NOT NULL,
  `user_has_articles_user_iduser` int(11) NOT NULL,
  `user_has_articles_articles_idarticles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `idrole` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`idrole`, `role`) VALUES
(1, 'admin'),
(3, 'utilisateur inscrit'),
(4, 'modérateur'),
(5, 'rédacteur');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `idrole` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `pseudo` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` char(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`iduser`, `idrole`, `name`, `firstName`, `pseudo`, `email`, `password`, `created_at`, `modified_at`) VALUES
(1, 0, 'bono', 'jean', 'jambon', 'jeanjean@g.com', '1230', '2022-08-01 15:09:27', '0000-00-00 00:00:00'),
(2, 0, 'bar', 'lenny', 'lolotte', 'nene@g.com', '1230', '2022-08-02 15:09:47', '0000-00-00 00:00:00'),
(3, 0, 'michee', 'sabrina', ' sabri', 'sab@g.com', '1230', '2022-08-08 15:10:07', '0000-00-00 00:00:00'),
(5, 0, 'Cesar', 'Jules', 'Ciji', 'ciji@gmail.com', '1230', '2022-08-09 15:10:21', '2022-08-11 11:56:33'),
(6, 0, 'zetofret', 'annie', 'ricard', 'picole@gmail.com', '1230', '2022-08-04 15:10:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user_has_articles`
--

CREATE TABLE `user_has_articles` (
  `user_iduser` int(11) NOT NULL,
  `articles_idarticles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idarticles`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`idcomments`),
  ADD KEY `fk_comments_articles1_idx` (`articles_idarticles`),
  ADD KEY `fk_comments_comments1_idx` (`comments_idcomments`),
  ADD KEY `fk_comments_user_has_articles1_idx` (`user_has_articles_user_iduser`,`user_has_articles_articles_idarticles`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`idrole`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- Index pour la table `user_has_articles`
--
ALTER TABLE `user_has_articles`
  ADD PRIMARY KEY (`user_iduser`,`articles_idarticles`),
  ADD KEY `fk_user_has_articles_articles1_idx` (`articles_idarticles`),
  ADD KEY `fk_user_has_articles_user1_idx` (`user_iduser`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `idarticles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `idcomments` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `idrole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_comments_articles1` FOREIGN KEY (`articles_idarticles`) REFERENCES `articles` (`idarticles`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comments_comments1` FOREIGN KEY (`comments_idcomments`) REFERENCES `comments` (`idcomments`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comments_user_has_articles1` FOREIGN KEY (`user_has_articles_user_iduser`,`user_has_articles_articles_idarticles`) REFERENCES `user_has_articles` (`user_iduser`, `articles_idarticles`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `user_has_articles`
--
ALTER TABLE `user_has_articles`
  ADD CONSTRAINT `fk_user_has_articles_articles1` FOREIGN KEY (`articles_idarticles`) REFERENCES `articles` (`idarticles`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_articles_user1` FOREIGN KEY (`user_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
