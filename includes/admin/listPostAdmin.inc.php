<?php

$title = "listPost";
?>
<?php
$pdo = pdo();
// requete pour selectionner tous les éléments de la bdd et les ordonner par date de création
$select_articles = "SELECT * FROM articles ORDER BY created_at DESC";
// prepare la requete à l'éxecution et repour un objet
$query = $pdo->query($select_articles);
// execute la requete
$query->execute();
// retourne tous les éléments et les affiche
$articles = $query->fetchAll();
?>
<!-- tableau affichant la réponse en html -->
<h1>Liste des articles</h1>
<table>
    <thead>
        <tr class="listTab">

            <th class="listcolum">title</th>
            <th class="listcolum">content</th>
            <th class="listcolum">status</th>

        </tr>
    </thead>
    <tbody>
        <!-- pour chaque reponse afficher les paramètres demandé ici id, nom, prenom ... -->
        <?php foreach ($articles as $article) { ?>
            <tr>

                <td class="listrow"><?= $article['title'] ?></td>
                <td class="listrow"><?= $article['content'] ?></td>
                <td class="listrow"><?= $article['status'] ?></td>
                <td class="listrow"><a href="index.php?page=singlePostAdmin&amp;idarticles=<?= $article['idarticles'] ?>">Afficher</a></td>
                <td class="listrow"><a href="index.php?page=updateArticleAdmin&amp;idarticles=<?= $article['idarticles'] ?>">Modifier</a></td>
                <td class="listrow"><a href="index.php?page=deleteArticleAdmin&amp;idarticles=<?= $article['idarticles'] ?>">Supprimer</a></td>
            </tr>

        <?php } ?>
    </tbody>
</table>
<button>
    <a href="index.php?page=newPost">Ajouter un article</a>
</button>