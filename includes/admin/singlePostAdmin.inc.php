<?php

if (!empty($_GET['idarticles']) && is_numeric($_GET['idarticles'])) {
    $pdo = pdo();
    $id = $_GET['idarticles'];
    $sql_edit_article = "SELECT * FROM articles WHERE idarticles = :id";
    $query = $pdo->prepare($sql_edit_article);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $article = $query->fetchAll();
    


    // En cas d'erreur retourne un tableau
    $errors = [];
    if (!empty($_POST['submitted'])) {

        // Faille XSS enlève les espace avec trim et les balises avec strip_tags pour eviter l'injection de code
        $title = trim(strip_tags($_POST['title']));
        $content = trim(strip_tags($_POST['content']));
        $status = trim(strip_tags($_POST['status']));
        // Validation
        $errors = validText($errors, $title, 'title', 2, 100);
        $errors = validText($errors, $content, 'content', 2, 100);
        $errors = validText($errors, $status, 'status', 2, 100);
    }

    // // Ajouter un commentaire
    // $sql = "SELECT * FROM comments WHERE idarticles = :id";
    // $query = $pdo->prepare($sql);
    // $query->bindValue(':id', $id, PDO::PARAM_INT);
    // $query->execute();
    // $comments = $query->fetchAll();



    $errors = [];
    if (!empty($_POST['submitted'])) {

        $content = trim(strip_tags($_POST['content']));
        $errors = validText($errors, $content, 'content', 2, 100);

        if (count($errors) == 0) {
            $sql = "INSERT INTO comments (id_article,content, created_at,modified_at,status)
            VALUES (:idarticle,:content,NOW(),NOW(),'new')";
            $query = $pdo->prepare($sql);
            $query->bindValue(':content', $content, PDO::PARAM_STR);
            $query->bindValue(':idarticles', $id, PDO::PARAM_INT);
            $query->execute();
            header('Location: singlePostAdmin.inc.php?id=' . $id);
            die();
        }
    }

?>
    <!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
    <h1>Article only</h1>
    <form action="" method="post" novalidate>
        <label for="title">
            <span>Titre:</span>
            <span type="text" name="title"> <?= $article[0]['title']; ?></span>
            <span class="error"><?php if (!empty($errors['title'])) {
                                    echo $errors['title'];
                                } ?></span>

        </label>
        <label for="content">
            <span>Contenu :</span>
            <span name="content" id="content" cols="30" rows="10"><?= $article[0]['content']; ?></span>
            <span class="error"><?php if (!empty($errors['content'])) {
                                    echo $errors['content'];
                                } ?></span>
        </label>
        <label for="status">
            <span>Statut :</span>
            <span type="text" name="status"> <?= $article[0]['status']; ?></ <span class="error"><?php if (!empty($errors['status'])) {
                                                                                                        echo $errors['status'];
                                                                                                    } ?></span>
        </label>

    </form>
    <!-- Ajouter un commentaire création d'un formulaire commentaire -->
    <h2>Ajouter un commentaire</h2>
    <form action="" method="post" class="wrap2">

        <label for="content">Commentaire </label>
        <textarea name="content" id="content2" cols="30" rows="10"></textarea>
        <span class="error"><?php if (!empty($errors['content'])) {
                                echo $errors['content'];
                            } ?></span>

        <input type="submit" name="submitted" value="Ajouter">
    </form>
    <!-- S'il n'y a pas d'absence de commentaire alors -->
    <!-- <?php if (!empty($comments)) { ?>
    <h2>Les commentaire</h2>
    <?php foreach ($comments as $comment) { ?>
        <div class="comment">
            <p><?= $comment['content']; ?></p>
            <hr>
        </div>
    <?php } ?>
<?php }
        } ?> -->
    </div>