<?php

if (!empty($_GET['iduser']) && is_numeric($_GET['iduser'])) {
    $pdo = pdo();
    $id = $_GET['iduser'];
    $sql_edit_user = "SELECT * FROM user WHERE iduser = :id";
    $query = $pdo->prepare($sql_edit_user);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $users = $query->fetch();


    $errors = [];


?>
    <!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
    <h1>C'est qui lui ?</h1>
    <form action="" method="post" novalidate>
        <label for="nom">
            <span>Nom:</span>
            <span type="text" name="nom"> <?= $users['name']; ?></span>
            <span class="error"><?php if (!empty($errors['name'])) {
                                    echo $errors['name'];
                                } ?></span>

        </label>

        <label for="firstName">
            <span>Prénom:</span>
            <span type="text" name="firstName"> <?= $users['firstName']; ?></span>
            <span class="error"><?php if (!empty($errors['firstName'])) {
                                    echo $errors['firstName'];
                                } ?></span>

        </label>

        <label for="pseudo">
            <span>Pseudo :</span>
            <span name="pseudo" id="pseudo"><?= $users['pseudo']; ?></span>
            <span class="error"><?php if (!empty($errors['pseudo'])) {
                                    echo $errors['pseudo'];
                                } ?></span>
        </label>
        <label for="email">
            <span>Email :</span>
            <span type="email" name="email"> <?= $users['email']; ?></ <span class="error"><?php if (!empty($errors['email'])) {
                                                                                                echo $errors['email'];
                                                                                            } ?></span>
        </label>
        <label for="role">
            <span>Rôle :</span>
            <span name="role" id="role"><?= $users['role']; ?></span>
            <span class="error"><?php if (!empty($errors['role'])) {
                                    echo $errors['role'];
                                } ?></span>
        </label>

    </form>
    <td class="listrow"><a href="index.php?page=controlUpdateUser&amp;iduser=<?= $users['iduser'] ?>">Modifier</a></td>
    <td class="listrow"><a href="index.php?page=controlDeleteUser&amp;iduser=<?= $users['iduser'] ?>">Supprimer</a></td>

<?php }
