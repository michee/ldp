<?php
if (!empty($_GET['category_id']) && is_numeric($_GET['category_id'])) {
    $pdo = pdo();
    $id = $_GET['category_id'];
    $sql_edit_cat = "SELECT * FROM category WHERE category_id = :id";
    $query = $pdo->prepare($sql_edit_cat);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $category = $query->fetch();
    var_dump($category);
    $errors = [];
    if (!empty($_POST['submitted'])) {

        // Faille XSS
        $cate = trim(strip_tags($_POST['category_id']));
        $nameCat = trim(strip_tags($_POST['name']));



        if (count($errors) === 0) {
            $requete_update = "UPDATE category SET category_id= :category_id, name=:name  WHERE category_id= :id";
            $query = $pdo->prepare($requete_update);
            $query->bindValue(':category_id', $category, PDO::PARAM_STR);
            $query->bindValue(':name', $nameCat, PDO::PARAM_STR);
            $query->execute();
            echo "<script>alert(`Catégorie modifié`)</script>";
            echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=categoryAdmin')</script>";
        }
    }
?>
    <!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
    <h1>Modifier la catégorie</h1>
    <form action="" method="post" novalidate>



        <label for="category">
            <span>Catégorie:</span>
            <input type="text" name="category" value="<?= $category['name'] ?>">
            <span class="error"><?php if (!empty($errors['name'])) {
                                    echo $errors['name'];
                                } ?></span>

        </label>


        <input type="submit" name="submitted" value="modifier">
    </form>

<?php } ?>