<?php
// vérification de l'ID dans la bdd
if (!empty($_GET['idarticles']) && is_numeric($_GET['idarticles'])) {
   $pdo = pdo();
   $id = $_GET['idarticles'];
   $sql_edit_article = "SELECT * FROM articles WHERE idarticles = :id";
   $query = $pdo->prepare($sql_edit_article);
   $query->bindValue(':id', $id, PDO::PARAM_INT);
   $query->execute();
   $article = $query->fetch();

   //  verification si l'id dans l'URL existe. s'il existe on fait une requete à la bdd delete
   if (!empty($_GET['idarticles']) && is_numeric($_GET['idarticles'])) {
      $id = $_GET['idarticles'];
      $pdo = pdo();
      // requete bdd
      $sql_supp = "DELETE  FROM articles WHERE idarticles = :id";
      // on prepare une requête à l'exécution et retourne un objet
      $query = $pdo->prepare($sql_supp);
      //  on associe une valeur à un paramètre
      $query->bindValue(':id', $id, PDO::PARAM_INT);
      // exécution de la requete
      $query->execute();
      // une fois la requete executé on retourne sur une autre page
      echo "<script>alert(`vous avez bien supprimé l'article`)</script>";
      echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=listPostAdmin')</script>";
   }
} else {
   // si erreur on arrete le code avec message d'erreur
   die("requete impossible. il y a une erreur");
}
