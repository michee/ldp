<?php

$pdo = pdo();

$cat = "SELECT * FROM `category`";
$categories = $pdo->query($cat)->fetchAll();
?>
<h1>Liste des catégories</h1>
<table>
    <thead>
        <tr class="listTab">

            <th class="listcolum">catégorie</th>


        </tr>
    </thead>
    <tbody>
        <!-- pour chaque reponse afficher les paramètres demandé ici id, nom, prenom ... -->
        <?php foreach ($categories as $category) { ?>
            <tr>

                <td class="listrow"><a href="index.php?page=listPostAdmin"><?= $category['name'] ?></a></td>
                <td class="listrow"><a href="index.php?page=singleCategoryAdmin&amp;category_id=<?= $category['category_id'] ?>">Afficher</a></td>
                <td class="listrow"><a href="index.php?page=categoryUpdateAdmin&amp;category_id=<?= $category['category_id'] ?>">Modifier</a></td>
                <td class="listrow"><a href="index.php?page=deleteCategoryAdmin&amp;category_id=<?= $category['category_id'] ?>">Supprimer</a></td>


            </tr>

        <?php } ?>
    </tbody>
</table>