<?php
// vérification de l'ID dans la bdd
if (!empty($_GET['iduser']) && is_numeric($_GET['iduser'])) {
   $pdo = pdo();
   $id = $_GET['iduser'];
   $sql_edit_user = "SELECT * FROM user WHERE iduser = :id";
   $query = $pdo->prepare($sql_edit_user);
   $query->bindValue(':id', $id, PDO::PARAM_INT);
   $query->execute();
   $users = $query->fetch();

   //  verification si l'id dans l'URL existe. s'il existe on fait une requete à la bdd delete
   if (!empty($_GET['iduser']) && is_numeric($_GET['iduser'])) {
      $id = $_GET['iduser'];
      $pdo = pdo();
      // requete bdd
      $sql_supp = "DELETE  FROM user WHERE iduser = :id";
      // on prepare une requête à l'exécution et retourne un objet
      $query = $pdo->prepare($sql_supp);
      //  on associe une valeur à un paramètre
      $query->bindValue(':id', $id, PDO::PARAM_INT);
      // exécution de la requete
      $query->execute();
      // une fois la requete executé on retourne sur une autre page
      echo "<script>alert(`vous avez bien supprimé cet utilisateur`)</script>";
      echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=controlListUser')</script>";
   }
} else {
   // si erreur on arrete le code avec message d'erreur
   die("requete impossible. il y a une erreur");
}
