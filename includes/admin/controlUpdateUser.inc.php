<?php
if (!empty($_GET['iduser']) && is_numeric($_GET['iduser'])) {
    $pdo = pdo();
    $id = $_GET['iduser'];
    $sql_edit_user = "SELECT * FROM user  WHERE iduser = :id";
    $query = $pdo->prepare($sql_edit_user);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $user = $query->fetch();
    $roles = "SELECT * from role";
    $roleChoice = $pdo->query($roles)->fetchAll();
    $status = array(
        $roleChoice[0]['role'],
        $roleChoice[1]['role'],
        $roleChoice[2]['role'],
        $roleChoice[3]['role']
    );
    $errors = [];
    if (!empty($_POST['submitted'])) {

        // Faille XSS
        $name = trim(strip_tags($_POST['name']));
        $firstName = trim(strip_tags($_POST['firstName']));
        $pseudo = trim(strip_tags($_POST['pseudo']));
        $email = trim(strip_tags($_POST['email']));
        $role = trim(strip_tags($_POST['role']));
        // Validation
        $errors = validText($errors, $name, 'name', 2, 100);
        $errors = validText($errors, $firstName, 'firtName', 2, 1000);
        $errors = validTExt($errors, $pseudo, 'pseudo', 2, 100);
        $errors = validTExt($errors, $email, 'email', 2, 100);


        if (count($errors) === 0) {
            $requete_update = "UPDATE user SET name= :name, firstName= :firstName, pseudo= :pseudo, email=:email, role= :role , modified_at=NOW()  WHERE iduser= :id";
            $query = $pdo->prepare($requete_update);
            $query->bindValue(':name', $name, PDO::PARAM_STR);
            $query->bindValue(':firstName', $firstName, PDO::PARAM_STR);
            $query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
            $query->bindValue(':email', $email, PDO::PARAM_STR);
            $query->bindValue(':role', $role, PDO::PARAM_INT);
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            echo "<script>alert(`Données utilisateur modifiées`)</script>";
            echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=controlListUser')</script>";
        }
    }
?>
    <!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
    <h1>Modifier l'utilisateur</h1>
    <form action="" method="post" novalidate>

        <label for="name">
            <span>Nom:</span>
            <input type="text" name="name" value="<?= $user['name'] ?>">
            <span class="error"><?php if (!empty($errors['name'])) {
                                    echo $errors['name'];
                                } ?></span>

        </label>
        <label for="firstName">
            <span>Prénom:</span>
            <input type="text" name="firstName" value="<?= $user['firstName'] ?>">
            <span class="error"><?php if (!empty($errors['firstName'])) {
                                    echo $errors['firstName'];
                                } ?></span>

        </label>
        <label for="pseudo">
            <span>Pseudo:</span>
            <input type="text" name="pseudo" value="<?= $user['pseudo'] ?>">
            <span class="error"><?php if (!empty($errors['pseudo'])) {
                                    echo $errors['pseudo'];
                                } ?></span>

        </label>
        <label for="email">
            <span>email :</span>
            <input type="email" name="email" value="<?= $user['email'] ?>">
            <span class="error"><?php if (!empty($errors['email'])) {
                                    echo $errors['email'];
                                } ?></span>
        </label>

        <?php
        // pour offrir deux option à status création d'un tableau avec les  choix des roles



        var_dump($status);
        ?>
        <label for="role">Rôle :</label>
        <select name="role">
            <option value="">---------------------</option>
            <!-- faire une fonction  -->
            <?php foreach ($status as $key => $value) {
                $selected = '';
                if (!empty($_POST['role'])) {
                    if ($_POST['role'] == $key) {
                        $selected = 'selected="selected"';
                    }
                }
            ?>
                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
            <?php } ?>
        </select>
        <span class="error"><?php if (!empty($errors['role'])) {
                                echo $errors['role'];
                            } ?></span>

        </div>
        <input type="submit" name="submitted" value="modifier">
    </form>

<?php } ?>