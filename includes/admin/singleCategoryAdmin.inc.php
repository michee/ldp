
<?php

if (!empty($_GET['category_id']) && is_numeric($_GET['category_id'])) {
    
    $pdo = pdo();
    $id = $_GET['category_id'];
    $articles = "SELECT * FROM articles LEFT JOIN category ON articles.category_id = category.category_id  WHERE idarticles = :id";
    $query = $pdo->prepare($articles);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $article = $query->fetchAll();
    var_dump($article);


    // En cas d'erreur retourne un tableau
    $errors = [];
    if (!empty($_POST['submitted'])) {

        // Faille XSS enlève les espace avec trim et les balises avec strip_tags pour eviter l'injection de code
        $name = trim(strip_tags($_POST['name']));
        $title = trim(strip_tags($_POST['title']));
        $status = trim(strip_tags($_POST['status']));
        // Validation
        // $errors = validText($errors, $content, 'content', 2, 100);
        // $errors = validText($errors, $title, 'title', 2, 100);
        // $errors = validText($errors, $status, 'status', 2, 100);
    }

?>
    <!-- on edit par exemple l'utilisateur pour pouvoir proceder à la modification -->
    <h1>catégorie only</h1>
    <form action="" method="post" novalidate>
        <label for="name">
            <span>catégorie:</span>
            <span type="text" name="name"> <?= $article['name']; ?></span>
            <span class="error"><?php if (!empty($errors['name'])) {
                                    echo $errors['name'];
                                } ?></span>

            <?php foreach ($article as $art) { ?>
                <tr>
                    <td class="listrow"><?= $art['title'] ?></td>
                    <td class="listrow"><?= $art['content'] ?></td>
                    <td class="listrow"><?= $art['status'] ?></td>
                </tr>

            <?php } ?>
    </form>


    <button>
        <a href="index.php?page=newCategoryAdmin">Ajouter une nouvelle catégorie</a>
    </button>

<?php } ?>
