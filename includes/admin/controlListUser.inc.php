<?php
$pdo = pdo();
// requete pour selectionner tous les éléments de la bdd et les ordonner par date de création
$select_users = "SELECT * FROM user ORDER BY name DESC";
// prepare la requete à l'éxecution et repour un objet
$query = $pdo->query($select_users);
// execute la requete
$query->execute();
// retourne tous les éléments et les affiche
$users = $query->fetchAll();
?>
<!-- tableau affichant la réponse en html -->
<h1>Liste des utilisateurs</h1>
<table>
    <thead>
        <tr class="listTab">

            <th class="listcolum">Nom</th>
            <th class="listcolum">Prénom</th>
            <th class="listcolum">Pseudo</th>
            <th class="listcolum">email</th>
            <th class="listcolum">role</th>

        </tr>
    </thead>
    <tbody>
        <!-- pour chaque reponse afficher les paramètres demandé ici id, nom, prenom ... -->
        <?php foreach ($users as $user) { ?>
            <tr>

                <td class="listrow"><?= $user['name'] ?></td>
                <td class="listrow"><?= $user['firstName'] ?></td>
                <td class="listrow"><?= $user['pseudo'] ?></td>
                <td class="listrow"><?= $user['email'] ?></td>
                <td class="listrow"><?= $user['role'] ?></td>
                <td class="listrow"><a href="index.php?page=controlUser&amp;iduser=<?= $user['iduser'] ?>">afficher</a></td>
                <td class="listrow"><a href="index.php?page=controlUpdateUser&amp;iduser=<?= $user['iduser'] ?>">Modifier</a></td>
                <td class="listrow"><a href="index.php?page=controlDeleteUser&amp;iduser=<?= $user['iduser'] ?>">Supprimer</a></td>
            </tr>

        <?php } ?>
    </tbody>
</table>
<button>
    <a href="index.php?page=newPost">Ajouter un article</a>
</button>