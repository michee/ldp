<?php



// vérification de l'ID dans la bdd
if (!empty($_GET['category_id']) && is_numeric($_GET['category_id'])) {
   $pdo = pdo();
   $id = $_GET['category_id'];
   $sql_edit_cat = "SELECT * FROM category WHERE category_id = :id";
   $query = $pdo->prepare($sql_edit_cat);
   $query->bindValue(':id', $id, PDO::PARAM_INT);
   $query->execute();
   $cat = $query->fetch();

   //  verification si l'id dans l'URL existe. s'il existe on fait une requete à la bdd delete
   if (!empty($_GET['category_id']) && is_numeric($_GET['category_id'])) {
      $id = $_GET['category_id'];
      $pdo = pdo();
      // requete bdd
      $sql_supp = "DELETE  FROM category WHERE category_id = :id";
      // on prepare une requête à l'exécution et retourne un objet
      $query = $pdo->prepare($sql_supp);
      //  on associe une valeur à un paramètre
      $query->bindValue(':id', $id, PDO::PARAM_INT);
      // exécution de la requete
      $query->execute();
      // une fois la requete executé on retourne sur une autre page
      echo "<script>alert(`vous avez bien supprimé cette catégorie`)</script>";
      echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=categoryAdmin')</script>";
   }
} else {
   // si erreur on arrete le code avec message d'erreur
   die("requete impossible. il y a une erreur");
}
