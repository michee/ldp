<?php
if (!empty($_GET['idarticles']) && is_numeric($_GET['idarticles'])) {
    $id = $_GET['idarticles'];
    $pdo = pdo();
    $articles = "SELECT * FROM articles LEFT JOIN category ON articles.category_id = category.category_id  WHERE idarticles = :id";
    $query = $pdo->prepare($articles);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $article = $query->fetch();
    $cat = "SELECT * FROM `category`";
    $catChoix = $pdo->query($cat)->fetchAll();
    $errors = [];
    if (!empty($_POST['submitted'])) {

        // Faille XSS
        $category = trim(strip_tags($_POST['category_id']));
        $title = trim(strip_tags($_POST['title']));
        $content = trim(strip_tags($_POST['content']));
        $status = trim(strip_tags($_POST['status']));
        // Validation
        $errors = validText($errors, $title, 'title', 2, 100);
        $errors = validText($errors, $content, 'content', 2, 1000);
        $errors = validTExt($errors, $status, 'status', 2, 100);

        if (count($errors) === 0) {
            $requete_update = "UPDATE articles SET category_id= :category_id, title= :title, content= :content,  modified_at=NOW(), status = :status  WHERE idarticles= :id";
            $query = $pdo->prepare($requete_update);
            $query->bindValue(':category_id', $category, PDO::PARAM_STR);
            $query->bindValue(':title', $title, PDO::PARAM_STR);
            $query->bindValue(':content', $content, PDO::PARAM_STR);
            $query->bindValue(':status', $status, PDO::PARAM_STR);
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            echo "<script>alert(`Article modifié`)</script>";
            echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=listPostAdmin')</script>";
        }
    }
?>
    <!-- on edit par exemple l'utilisateur pour poouvoir proceder à la modification -->
    <h1>Modifier l'article</h1>
    <form action="" method="post" novalidate>
        <?php
        // pour offrir des ooptions création d'un tableau avec les choix multiples
        $status = array(
            $catChoix['0']['name'],
            $catChoix['1']['name'],
            $catChoix['2']['name'],
            $catChoix['3']['name'],
            $catChoix['4']['name'],
        );

        ?>
        <label for="categorie">Catégories :</label>
        <select name="categorie">
            <option value="">---------------------</option>
            <!-- faire une fonction  -->
            <?php foreach ($status as $key => $value) {
                $selected = '';
                if (!empty($_POST['name'])) {
                    if ($_POST['name'] == $key) {
                        $selected = 'selected="selected"';
                    }
                }
            ?>
                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
            <?php } ?>
        </select>
        <span class="error"><?php if (!empty($errors['name'])) {
                                echo $errors['name'];
                            } ?></span>

        </div>
        <label for="title">
            <span>Titre:</span>
            <input type="text" name="title" value="<?= $article['title'] ?>">
            <span class="error"><?php if (!empty($errors['title'])) {
                                    echo $errors['title'];
                                } ?></span>

        </label>
        <label for="content">
            <span>Contenu :</span>
            <textarea name="content" id="content" cols="30" rows="10"><?= $article['content'] ?></textarea>
            <span class="error"><?php if (!empty($errors['content'])) {
                                    echo $errors['content'];
                                } ?></span>
        </label>

        <?php
        // pour offrir deux option à status création d'un tableau avec les 2 choix
        $status = array(
            'prive' => 'Privé',
            'public' => 'Public'
        );
        ?>
        <select name="status">
            <option value="">---------------------</option>
            <!-- faire une fonction  -->
            <?php foreach ($status as $key => $value) {
                $selected = '';
                if (!empty($_POST['status'])) {
                    if ($_POST['status'] == $key) {
                        $selected = ' selected="selected"';
                    }
                }
            ?>
                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
            <?php } ?>
        </select>
        <span class="error"><?php if (!empty($errors['status'])) {
                                echo $errors['status'];
                            } ?></span>
        <input type="submit" name="submitted" value="modifier">
    </form>

<?php } ?>