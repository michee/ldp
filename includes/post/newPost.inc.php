<?php
// importer les fonctions et le lien avec la bdd
$title = "newPost";
// Traitement PHP
// Formulaire est soumis ???
$success = false;
// création d'un tableau d'erreur

$pdo = pdo();
$cat = "SELECT * FROM `category`";
$catChoix = $pdo->query($cat)->fetchAll();
var_dump($catChoix);
$errors = [];
// vérifier si il y a un post submit
if (!empty($_POST['submitted'])) {
    // Faille XSS trim pour enlever les espaces et strip_tags pour enlever les balises pour éviter l'injection de code
    $category = trim(strip_tags($_POST['category_id']));
    $title = trim(strip_tags($_POST['title']));
    $content = trim(strip_tags($_POST['content']));
    $status = trim(strip_tags($_POST['status']));
    // Validation
    $errors = validText($errors, $title, 'title', 2, 30);
    $errors = validText($errors, $content, 'content', 2, 1000);
    $errors = validText($errors, $status, 'status', 2, 30);


    if (count($errors) === 0) {
        $pdo = pdo();
        // insertion en BDD si aucune error en envoyant à la dbb une requete
        $sql = "INSERT INTO articles ( title, content,  created_at, modified_at, status, category_id)  VALUES ( :title, :content, NOW(), NOW(), :status, :category_id);";
        // INJECTION SQL
        // prépare une requête à l'exécution et retourne un objet
        $query = $pdo->prepare($sql);
        // associe une valeur à un parametre
        $query->bindValue(':category_id', $category, PDO::PARAM_STR);
        $query->bindValue(':title', $title, PDO::PARAM_STR);
        $query->bindValue(':content', $content, PDO::PARAM_STR);
        $query->bindValue(':status', $status, PDO::PARAM_STR);
        //  execute la requete
        $query->execute();
        // Retourne l'identifiant de la dernière ligne insérée ou la valeur d'une séquence
        $last_id = $pdo->lastInsertId();
        // nvoie sur une autre page une fois la requete exécutée
        echo "<script>alert(`Article ajouté`)</script>";
        echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=listPost')</script>";
    }
    $success = true;
}
?>


<!-- création d'un formulaire html -->
<h1>Ajouter un article &#128526;</h1>
<form action="" method="post" novalidate>
    <?php
    $categorie = array(
        $catChoix['0']['name'],
        $catChoix['1']['name'],
        $catChoix['2']['name'],
        $catChoix['3']['name'],
        $catChoix['4']['name'],
    );

    ?>
    <label for="category">Catégories :</label>
    <select name="category">
        <option value="">---------------------</option>
        <!-- faire une fonction  -->
        <?php foreach ($categorie as $key => $value) {
            $selected = '';
            if (!empty($_POST['name'])) {
                if ($_POST['name'] == $key) {
                    $selected = ' selected="selected"';
                }
            }
        ?>
            <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
        <?php } ?>
    </select>
    <span class="error"><?php if (!empty($errors['name'])) {
                            echo $errors['name'];
                        } ?></span>

    </div>
    <label for="title">Titre</label>
    <input type="text" name="title" required id="title" value="<?php if (!empty($_POST['title'])) {
                                                                    echo $_POST['title'];
                                                                } ?>">
    <span class="error"><?php if (!empty($errors['title'])) {
                            echo $errors['title'];
                        } ?></span>

    <label for="content">Contenu</label>
    <textarea name="content" id="content" cols="30" rows="10"><?php if (!empty($_POST['content'])) {
                                                                    echo $_POST['content'];
                                                                } ?></textarea>
    <span class="error"><?php if (!empty($errors['content'])) {
                            echo $errors['content'];
                        } ?></span>
    <?php
    // pour offrir deux option à status création d'un tableau avec les 2 choix
    $status = array(
        'prive' => 'Privé',
        'public' => 'Public'
    );

    ?>
    <select name="status">
        <option value="">---------------------</option>
        <!-- faire une fonction  -->
        <?php foreach ($status as $key => $value) {
            $selected = '';
            if (!empty($_POST['status'])) {
                if ($_POST['status'] == $key) {
                    $selected = ' selected="selected"';
                }
            }
        ?>
            <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
        <?php } ?>
    </select>
    <span class="error"><?php if (!empty($errors['status'])) {
                            echo $errors['status'];
                        } ?></span>



    <input type="submit" name="submitted" value="Ajouter cet article &#128523;">
</form>