<form action="index.php?page=signup" method="post">
    <div>
        <label for="name">Nom :</label>
        <input type="text" id="name" name="name" value="<?= $name ?>" />
    </div>
    <div>
        <label for="firstName">Prénom :</label>
        <input type="text" id="firstName" name="firstName" value="<?= $firstName ?>" />
    </div>
    <div>
        <label for="pseudo">Pseudo :</label>
        <input type="text" id="pseudo" name="pseudo" value="<?= $pseudo ?>" />
    </div>
    <div>
        <label for="email">E-mail :</label>
        <input type="text" id="email" name="email" value="<?= $email ?>" />
    </div>
    <div>
        <label for="password1">Mot de passe :</label>
        <input type="password" id="password1" name="password1" />
    </div>
    <div>
        <label for="password2">Vérification mot de passe :</label>
        <input type="password" id="password2" name="password2" />
    </div>
    <div>
        <input type="checkbox" name="cgu" id="cgu" value="1" <?= isset($_POST['cgu']) ? "checked" : ''; ?> /><label for="cgu">J'accepte les <a href="index.php?page=cgu" target="_blank">Conditions Générales d'Utilisation</a></label>
    </div>
    <div>
        <input type="reset" value="Effacer" />
        <input type="submit" value="Envoyer" />
    </div>
    <input type="hidden" name="signupForm" />
</form>