<h1>Inscription</h1>
<?php

if (isset($_POST['signupForm'])) {
    $name = isset($_POST['name']) ? htmlentities(trim($_POST['name'])) : "";
    $firstName = isset($_POST['firstName']) ? htmlentities(trim($_POST['firstName'])) : "";
    $pseudo = isset($_POST['pseudo']) ? htmlentities(trim($_POST['pseudo'])) : "";
    $email = isset($_POST['email']) ? htmlentities(trim($_POST['email'])) : "";
    $password1 = isset($_POST['password1']) ? htmlentities(trim($_POST['password1'])) : "";
    $password2 = isset($_POST['password2']) ? htmlentities(trim($_POST['password2'])) : "";
    $cgu = isset($_POST['cgu']) ? $_POST['cgu'] :  "";


    $erreurs = array();

    if (mb_strlen($name) === 0)
        array_push($erreurs, "Veuillez saisir votre nom");

    if (mb_strlen($firstName) === 0)
        array_push($erreurs, "Veuillez saisir votre prénom");

    if (mb_strlen($pseudo) === 0)
        array_push($erreurs, "Veuillez saisir votre prénom");

    if (mb_strlen($email) === 0)
        array_push($erreurs, "Veuillez saisir une adresse mail");

    elseif (!(filter_var($email, FILTER_VALIDATE_EMAIL)))
        array_push($erreurs, "Veuillez saisir une adresse mail conforme");

    if (mb_strlen($password1) === 0 || mb_strlen($password2) === 0)
        array_push($erreurs, "Veuillez saisis deux fois votre mot de passe");

    elseif ($password1 !== $password2)
        array_push($erreurs, "Vos mots de passe ne sont pas identiques");

    if (empty($_POST['cgu']))
        array_push($erreurs,  "Vous n'êtes pas d'accord avec les conditions de service");

    if (count($erreurs) > 0) {
        $messageErreurs = "<ul>";

        for ($i = 0; $i < count($erreurs); $i++) {
            $messageErreurs .= "<li>";
            $messageErreurs .= $erreurs[$i];
            $messageErreurs .= "</li>";
        }

        $messageErreurs .= "</ul>";

        echo $messageErreurs;


        // Vérification de l'inscription préalable ou non de l'utilisateur
        if (verifUser($email)) {
            // La fonction verifierUtilisateur() renvoie vrai (il y a déjà une ligne avec cette adresse), pas de traitement
            echo "Vous êtes déjà inscrit";
        } 
        else {
        // La fonction verifierUtilisateur() renvoie faux, donc on procède à l'inscription
        if (inscrireUtilisateur($name, $firstName, $pseudo, $email, $password1))
            $message = "Utilisateur inscrit";
        else
            $message = "Erreur";

        echo $message;

        echo "<script>window.location.replace('http://localhost/evaluationPhp/ldp/index.php?page=signup')</script>";
    }
    }
} else {
    $name = $firstName = $pseudo = $email = $cgu = "";
    require_once 'signupForm.php';
}
