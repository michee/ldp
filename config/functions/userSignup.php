<?php

function inscrireUtilisateur(string $name, string $firstName, string $pseudo, string $email, string $password1): bool
{
    $password = password_hash($password1, PASSWORD_DEFAULT);
    $pdo = pdo();
    if ($pdo) {

        $requeteInscription = "INSERT INTO user (name, firstName, pseudo, email, password, created_at) VALUES ( ':name', ':firstName', ':pseudo', ':email', ':password1', :NOW)";

        $query = $pdo->prepare($requeteInscription);
        $query->bindValue(':name', $name, PDO::PARAM_STR);
        $query->bindValue(':firstName', $firstName, PDO::PARAM_STR);
        $query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
        $query->bindValue(':email', $email, PDO::PARAM_STR);
        $query->bindValue(':password1', $password, PDO::PARAM_STR);
        $query->execute();

        return true;
    } else {
        return false;
    }
}
