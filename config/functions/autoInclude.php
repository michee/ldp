<?php

function autoInclude(string $file): void
{
    // Récupération de tous les fichiers du répertoire 'user' qui ont la double extension .inc.php
    $includedUserFiles = glob("./includes/user/*.inc.php");
    // Récupération de tous les fichiers du répertoire 'post' qui ont la double extension .inc.php
    $includedPostFiles = glob("./includes/post/*.inc.php");
    // Récupération de tous les fichiers du répertoire 'post' qui ont la double extension .inc.php
    $includedAdminFiles = glob("./includes/admin/*.inc.php");

    // Concaténation du nom de fichier avec le chemin et l'extension
    $userFile = "./includes/user/" . $file . ".inc.php";
    $postFile = "./includes/post/" . $file . ".inc.php";
    $adminFile = "./includes/admin/" . $file . ".inc.php";
    // Si le nombre de fichiers dans le tableau est > 0 et que la chaîne de caractères $files est dans le tableau
    if (count($includedUserFiles) !== 0 && in_array($userFile, $includedUserFiles)) {
        require_once $userFile;
    } else {
        require_once './includes/accueil.inc.php';
    }
    if (count($includedPostFiles) !== 0 && in_array($postFile, $includedPostFiles)) {
        require_once $postFile;
    } else {
        require_once './includes/accueil.inc.php';
    }
    if (count($includedAdminFiles) !== 0 && in_array($adminFile, $includedAdminFiles)) {
        require_once $adminFile;
    } else {
        require_once './includes/accueil.inc.php';
    }



    // Récupération de tous les fichiers du répertoire 'includes' qui ont la double extension .inc.php
    // $includedFiles = glob("./includes/*.inc.php");
    // // Concaténation du nom de fichier avec le chemin et l'extension
    // $file = "./includes/" . $file . ".inc.php";

    // // Si le nombre de fichiers dans le tableau est > 0 et que la chaîne de caractères $files est dans le tableau
    // if (count($includedFiles) !== 0 && in_array($file, $includedFiles)) {
    //     require_once $file;
    // } else {
    //     require_once './includes/accueil.inc.php';
    // }
}


function autoIncludePost(string $file): void
{

    // Récupération de tous les fichiers du répertoire 'post' qui ont la double extension .inc.php
    $includedPostFiles = glob("./includes/post/*.inc.php");
    // Concaténation du nom de fichier avec le chemin et l'extension
    $file = "./includes/post/" . $file . ".inc.php";

    // Si le nombre de fichiers dans le tableau est > 0 et que la chaîne de caractères $files est dans le tableau

    if (count($includedPostFiles) !== 0 && in_array($file, $includedPostFiles)) {
        require_once $file;
    } else {
        require_once './includes/accueil.inc.php';
    }
}
